package interfaces;

import java.io.IOException;

import my.logger.Event;

public interface EventLogger {
	void logEvent(Event event) throws IOException;
}
