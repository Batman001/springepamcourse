package my.app;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import interfaces.EventLogger;
import model.Client;
import my.logger.Event;
public class App {
	Client client;
	EventLogger logger;
	
	public App(Client client, EventLogger logger) {
		super();
		this.client = client;
		this.logger = logger;
	}

	public App() {
		// TODO Auto-generated constructor stub
	}

	public void logEvent(Event event) throws IOException {
		//String message=msg.replaceAll(client.getId(), client.getFullName());
		logger.logEvent(event);
	}

	public static void main(String[] args) {

		ApplicationContext applicationContext= new ClassPathXmlApplicationContext("spring.xml");
		App app=(App) applicationContext.getBean("app");
		Event event= new Event(new Date());
		event.setMsg("1");
		try {
			app.logEvent(event);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
