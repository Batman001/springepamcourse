package my.logger;

import java.time.LocalDate;
import java.util.Date;
import java.util.Random;

public class Event {
	private int id;
	private String msg;
	private Date date;


	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Event() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Event(Date date) {
		Random rand = new Random();
		this.id = rand.nextInt(rand.nextInt(1000));
		this.date = date;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", msg=" + msg + ", date=" + date + "," + "dateFormat=" + "]";
	}

}
