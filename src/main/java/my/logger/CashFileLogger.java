package my.logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CashFileLogger extends FileEventLogger {
	private int cashSize;
	List<Event> cash = new ArrayList<>();

	public CashFileLogger(int cashSize, String fileName) {
		super(fileName);
		this.cashSize = cashSize;
		// TODO Auto-generated constructor stub
	}

	public void logEvent(Event event) throws IOException {
		cash.add(event);
		if (cash.size() == cashSize) {
			writeEventsFromCash();
			cash.clear();
		}

	}

	public void writeEventsFromCash() throws IOException {
		for (Event elem : cash) {
			super.logEvent(elem);
		}
	}

}
