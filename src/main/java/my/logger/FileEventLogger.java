package my.logger;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import interfaces.EventLogger;

public class FileEventLogger implements EventLogger {
	private String fileName;
	private File file;

	public FileEventLogger(String fileName) {
		super();
		this.fileName = fileName;
	}

	public void init() throws IOException {
		this.file = new File(fileName);
		/*if (file.canWrite())
			throw new IOException();*/
	}

	public void logEvent(Event event) throws IOException {

		FileUtils.writeStringToFile(file, event.getMsg(), true);

	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
